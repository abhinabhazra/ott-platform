import { BrowserRouter, Route, Switch } from "react-router-dom";
import "./App.css";
import React, { useState } from 'react'
import Header from "./components/Header/Header";
import SimpleBottomNavigation from "./components/MainNav";
import Movies from "./Pages/Movies/Movies";
import Series from "./Pages/Series/Series";
import Trending from "./Pages/Trending/Trending";
import Search from "./Pages/Search/Search";
import { Container } from "@material-ui/core";
import Login from "./Pages/Login/Login";
import Producer from "./Pages/Producer/Producer";
import Add_movies from "./Pages/Add_movies/Add_movies";
import My_movies from "./Pages/My_movies/My_movies";
import Producer_movies from "./Pages/Producer_movies/Producer_movies";
import User_profile from "./Pages/User_profile/User_profile";







const LoginModal = ({close, login}) => (
  <div className="modal-bg">
    <div className="modal">
      <Login close={close} login={login} />
    </div>
  </div>
)

function App() {
  const [loginModal, setLoginModal] = useState(false);
  const [login, setLogin] = useState(true);
  const [isLogin, setIsLogin] = useState(false);
  const showLoginModal = (signup) => {
    setLoginModal(() => {
      setLogin(signup);
      return true;
    })
  }

  const getUserState = () => {
   console.log('ssss');
    setIsLogin(true)
  }

  const closeModal = () => {
    setLoginModal(false)
  }
  return (
    <BrowserRouter>
      <Header showLogin={showLoginModal}
      />
      { loginModal ? <LoginModal close={closeModal} login={login} getUserState={getUserState}/> : null }
      <div className="app">
        <Container>
          <Switch>
            <Route path="/" component={Trending} exact />
            <Route path="/movies" component={Movies} />
            <Route path="/series" component={Series} />
            <Route path="/search" component={Search} />
            <Route path="/producer" component={Producer}/>
            <Route path="/add_movies" component={Add_movies}/>
            <Route path="/my_movies" component={My_movies}/>
            <Route path="/producer_movies" component={Producer_movies}/>
            <Route path="/user_profile" component={User_profile}/>






          </Switch>
        </Container>
      </div>
      <SimpleBottomNavigation />
    </BrowserRouter>
  );
}

export default App;
