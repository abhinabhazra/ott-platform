import React, { useEffect, useState } from "react";
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { Link } from "react-router-dom";
import MovieIcon from '@material-ui/icons/Movie';
import "./Producer_movies.css";
import axios from "axios";





function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`nav-tabpanel-${index}`}
      aria-labelledby={`nav-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `nav-tab-${index}`,
    'aria-controls': `nav-tabpanel-${index}`,
  };
}

function LinkTab(props) {
    return (
      <Tab
        component="a"
        onClick={(event) => {
          event.preventDefault();
        }}
        {...props}
      />
    );
  }
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
  table: {
    minWidth: 650,
},
}));

export default function SimpleTabs() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

   //for get services
   const [movieDetails, setStateMovieDetails] = useState([]);
   //const [txt_user_created, setCreater] = useState([]);




   useEffect(() => {
       if(sessionStorage.getItem('auth')){
      //    let {txt_user_id } = JSON.parse(sessionStorage.getItem('auth'));
      //    let user_created =  {'user_created':txt_user_id};
      //    console.log(user_created)
      //     axios.get('http://13.126.196.58:8082/api/movieListByUser',user_created).then(res => {
      //       //setTitle(res.data.response.txt_movie_title)
      //       setStateMovieDetails(res.data.response)
      //     console.log(res.data.response)
      //  }).catch(err=> console.log(err))
      let {txt_user_id } = JSON.parse(sessionStorage.getItem('auth'));
      let user_created =  {'user_created':txt_user_id};
      let result= fetch("http://13.126.196.58:8082/api/movieListByUser",{
          method:'POST',
          body:JSON.stringify(user_created),
          headers:{
            "Content-Type":'application/json',
          }
        })
        .then((resp) => {
          return resp.json();
        }).then((data) => {
          setStateMovieDetails(data.response)
        })
       }

           
           

       
      

   }, [])

  return (
    <div className={classes.root}>
      <AppBar position="static">
      <Tabs
          variant="fullWidth"
          value={value}
          onChange={handleChange}
          aria-label="nav tabs example"
        >
          <LinkTab className="link_tab" icon={<MovieIcon/>} label="My Movies" {...a11yProps(0)} />
          <LinkTab  label="Pending/Review Movies"  icon={<MovieIcon/>} className="link_tab" {...a11yProps(1)} />
          <LinkTab label="Rejected Movies"  icon={<MovieIcon/>} className="link_tab" {...a11yProps(2)} />
        </Tabs>
      </AppBar>
      <TabPanel value={value} index={0}>
      <TableContainer component={Paper}>
            <Table className={classes.table} aria-label="simple table">
                <TableHead>
                    <h3>Producer Movie Table</h3>
                    <TableRow className="colu">
                        <TableCell>Movie ID</TableCell>
                        <TableCell align="left">Movie Title</TableCell>
                        <TableCell align="left">Genre</TableCell>
                        <TableCell align="left">Producer</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {movieDetails && movieDetails.map((row,i) => (
                        <TableRow key={i}>
                            <TableCell component="th" scope="row">
                                {i + 1}
                            </TableCell>
                            <TableCell align="left">{row.txt_movie_title}</TableCell>
                            <TableCell align="left">{row.genre}</TableCell>
                            <TableCell align="left">{row.txt_producer}</TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>      </TabPanel>
      <TabPanel value={value} index={1}>
      No movies under Admin Bucket for Approval.
      </TabPanel>
      <TabPanel value={value} index={2}>
      No movies under Admin Bucket for Rejection.
   </TabPanel>
    </div>
  );
}

