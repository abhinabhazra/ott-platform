import React from "react";
import { Form, Input } from "usetheform";
import "./Add_movies.css";
import {Link} from "react-router-dom";


const WizardFormFirstPage = (props) => (
    <div className="main">
        <Form name="page1" {...props}>
            <h2>ADD NEW MOVIES</h2>
            <hr />
            <p>Read these before you start. You will need to keep some image and video files ready.</p>
            <ul>
                <li>Type the movie name carefully. You are not allowed to change the movie name later.</li>
                <li>Select genre carefully. You are not allowed to change the genre later.</li>
                <li>Give proper censor data. Upload censor certificate if you have it.</li>
                <li>Synopsis is required. Providing synopsis will help us to better understand your movie.</li>
                <li>Cast and crew data is required. Providing cast-and-crew data will help us to understand the star-cast in your movie.</li>
                <li>One Horizontal Poster of size 950 (pixels) x 590 (pixels) is required.</li>
                <li>One Vertical Poster of size 630 (pixels) x 950 (pixels) is required.</li>
                <li>One Trailer file is mandatory.</li>
                <li>One Movie file is mandatory.</li>
            </ul>
            <hr />
            <button id="style_button" type="button"><Link to={"/"}>Back to Home</Link></button>  <button id="style_button" type="submit">OK , Proceed</button>
        </Form>
    </div>
)

export default WizardFormFirstPage;