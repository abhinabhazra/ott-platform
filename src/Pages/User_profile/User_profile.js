import React, { useEffect, useState } from "react";
import { Form, Input, Select } from "usetheform";
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import axios from "axios";
import "./User_profile.css";
import { makeStyles } from '@material-ui/core/styles';
import EditIcon from '@material-ui/icons/Edit';


const useStyles = makeStyles((theme) => ({
    container: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    textField: {
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(1),
      width: 200,
    },
  }));
export default function UserProfile() {
    const classes = useStyles();

   // for POST services 
   const [profile_pic, setProfilePic] = useState("")
   const [first_name, setFirstName] = useState("")
   const [last_name, setLastName] = useState("")
   const [email, setEmail] = useState("")
   const [mobile, setMobile] = useState("")
   const [dob, setDob] = useState("")
   const [gender, setGender] = useState("")
   const [add1, setAdd1] = useState("")
   const [add2, setAdd2] = useState("")
   const [pin, setPin] = useState("")
   const [addMovie_msg, setMsg] = useState("")



   //  first page/////////////////////////////////////////////////

   async function userPage(e) {
       e.preventDefault()
       let item = {
           profile_pic: profile_pic, first_name: first_name, last_name: last_name, email: email,
           mobile: mobile, dob: dob, gender: gender, add1: add1,
           add2: add2, pin: pin, 
       }

       console.warn(item)

       let result = await fetch("http://localhost:8082/api/user_profile", {
           method: 'POST',
           body: JSON.stringify(item),
           headers: {
               "Content-Type": 'application/json',
           }
       })
           .then((resp) => resp.json().then(r => {
               setMsg(r.message)
           }))
   }
    return (
        <Form>
            <h1>User Profile</h1>

            <Grid className="add_image" container spacing={2}>

            <Grid item xs={12}>
                    <div className="profile_image">
                    <img className="img_responsive" src="307658.jpg" alt="pancard" />
                    
                    <h3>Upload Profile Picture <EditIcon/></h3>
                    <TextField
                            type="file" accept="image/*" name="img" 
                            onChange={(e) => setProfilePic(e.target.value)}
                        />
                    </div>
                    
                </Grid>


                <Grid item xs={12} sm={6}>
                    <TextField
                        variant="outlined"

                        fullWidth
                        onChange={(e) => setFirstName(e.target.value)}

                        name="fName"
                        label="First Name"
                        type="fName"
                        placeholder="Enter First Name "
                        id="fName"
                        required
                        
                    />
                </Grid>
                <Grid item xs={12} sm={6}>
                    <TextField
                        variant="outlined"

                        fullWidth
                         onChange={(e) => setLastName(e.target.value)}

                        name="lName"
                        label="Last Name"
                        type="lName"
                        id="lName"
                        placeholder="Enter Last Name"

                    />
                </Grid>

                <Grid item xs={12} sm={6}>
                    <TextField
                        variant="outlined"

                        fullWidth
                         onChange={(e) => setEmail(e.target.value)}

                        name="email"
                        label="Email"
                        type="email"
                        id="email"
                        placeholder="Enter Email"
                        required

                    />
                </Grid>
                <Grid item xs={12} sm={6}>
                    <TextField
                        variant="outlined"

                        fullWidth
                     onChange={(e) => setMobile(e.target.value)}

                        name="mobile"
                        label="Mobile"
                        type="mobile"
                        id="mobile"
                        placeholder="Enter Mobile Number"
                    />
                </Grid>

                <Grid item xs={12} sm={6}>
                <form className={classes.container} noValidate>
  <TextField
    id="date"
    label="Date Of Birth"
    type="date"
    variant="outlined"
    placeholder="Enter Date Of Birth"

    onChange={(e) => setDob(e.target.value)}

    fullWidth
    InputLabelProps={{
      shrink: true,
    }}
  />
</form>
                </Grid>
                <Grid item xs={12} sm={6}>
                    <TextField
                        variant="outlined"

                        fullWidth
                        onChange={(e) => setGender(e.target.value)}

                        name="gender"
                        label="Gender"
                        type="gender"
                        placeholder="Enter Gender"

                        id="gender"
                    />
                </Grid>

                <Grid item xs={12} sm={6}>
                    <TextField
                        variant="outlined"

                        fullWidth
                       onChange={(e) => setAdd1(e.target.value)}

                        name="add1"
                        label="Address 1"
                        placeholder="Enter Address 1"

                        type="add1"
                        id="add1"
                    />
                </Grid>
                <Grid item xs={12} sm={6}>
                    <TextField
                        variant="outlined"

                        fullWidth
                        onChange={(e) => setAdd2(e.target.value)}

                        name="add2"
                        label="Address 2"
                        type="add2"
                        id="add2"
                        placeholder="Enter Address 2"

                    />
                </Grid>

                <Grid item xs={12} sm={6}>
                    <TextField
                        variant="outlined"

                        fullWidth
                     onChange={(e) => setPin(e.target.value)}

                        name="pin"
                        label="Pin Code"
                        type="pin"
                        placeholder="Enter Pin Code"

                        id="pin"
                    />
                </Grid>
                <h3 className="message">{addMovie_msg}</h3>

            </Grid>


            <button id="style_button" onClick={userPage} type="submit" >submit data</button>

           
        </Form>
    );
}