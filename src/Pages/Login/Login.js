import React, { useState } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import NativeSelect from '@material-ui/core/NativeSelect';


import "./Login.css";




// const [type, setType] = useState([]);

// useEffect(() => {
//   axios.get('http://13.126.196.58:8082/api/user_type_list').then(res => {
//     setType(res.data.response)
//   })


// }, [])

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const Login = ({ close, login, getUserState }) => {
  const classes = useStyles();
  console.log(login)
  const [showLogin, setShowLogin] = useState(login);
  const showSignupScreen = () => {
    setShowLogin(false)
  }

  const showLoginScreen = () => {
    setShowLogin(true)
  }
  const [first_name, setFirstName] = useState("")
  const [password, setPassword] = useState("")
  const [email, setEmail] = useState("")
  const [last_name, setLastName] = useState("")
  const [signup_msg, setMsg] = useState("")

  const [username, setUsername] = useState("")
  const [passwordIn, setPasswordIn] = useState("")
  const [signIn_msg, setSign] = useState("")




  //  sign up/////////////////////////////////////////////////

  async function signUp(e) {
    e.preventDefault()
    let item = { first_name: first_name, last_name: last_name, usertype: 2, password: password, email: email, type: 'email', gender: 'M', }

    console.warn(item);

    let result = await fetch("http://13.126.196.58:8082/api/user_signup", {
      method: 'POST',
      body: JSON.stringify(item),
      headers: {
        "Content-Type": 'application/json',
      }
    })
      .then((resp) => resp.json().then(r => {
        setMsg(r.message)
        setTimeout(() => {
          close();
        }, 3000)
      }))
  }

  // sign in /////////////////////////
  async function signIn(e) {
    e.preventDefault()
    let item = { username: username, password: passwordIn, 'usertype': 2 }

    console.warn(item)
    if (item.username !== '' && item.password !== '') {
      let result = await fetch("http://13.126.196.58:8082/api/user_signin", {
        method: 'POST',
        body: JSON.stringify(item),
        headers: {
          "Content-Type": 'application/json',
        }
      })
        .then((resp) => {
          return resp.json();
        }).then((data) => {
          if (data.status === 'success') {
            let userInfo = {
              token: data.response.token,
              name: `${data.response.first_name} ${data.response.last_name}`,
              num_master_id: data.response.num_master_id,
              num_user_id: data.response.num_user_id,
              txt_user_id: data.response.txt_user_id,
              txt_emailid: data.response.txt_emailid
            };
            sessionStorage.setItem('auth', JSON.stringify(userInfo));
            close();
            window.location.reload(true);
          } else {
            setSign('Username and Password not match');
          }
        })
    } else {
      setSign('Username and Password both are required');

    }
  }


  if (showLogin) {

    return <Container component="main" maxWidth="xs">

      <div className="modal-header">
        <span className="close" onClick={close}>&times;</span>
      </div>
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Log in
    </Typography>
        <form className={classes.form} noValidate>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="username"
            onChange={(e) => setUsername(e.target.value)}
            label="username"
            name="username"
            autoComplete="username"
            autoFocus
            required
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            onChange={(e) => setPasswordIn(e.target.value)}
            autoComplete="current-password"
            required
          />


          <FormControlLabel
            control={<Checkbox value="remember" color="primary" />}
            label="Remember me"
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            onClick={signIn}
          >
            Log in
      </Button>
          <p>      {signIn_msg}
          </p>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Log in using phone no
      </Button>

          <div className="google_button"><Button
            type="submit"
            fullWidth
            variant="contained"

            className={classes.submit}
          >
            <a _ngcontent-c10="" class="socialLoginGp" href="https://www.addatimes.com/auth/social?web=true&amp;provider=google&amp;redirectUrl=">
              <i _ngcontent-c10="" class="fa fa-google-plus"></i>
                Login with Google
              </a>          </Button></div>

          <div className="facebook_button"><Button
            type="submit"
            fullWidth
            variant="contained"

            className={classes.submit}
          >
            <a _ngcontent-c10="" class="socialLoginFb" href="https://www.addatimes.com/auth/social?web=true&amp;provider=facebook&amp;redirectUrl=">
              <i _ngcontent-c10="" class="fa fa-facebook"></i>

                Login with Facebook
              </a>          </Button></div>
          <Grid container>
            <Grid item xs>
              <Link href="#" variant="body2">
                Forgot password?
          </Link>
            </Grid>
            <Grid item>
              <Link onClick={showSignupScreen} variant="body2">
                {"Don't have an account? Sign Up"}
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
      <Box mt={8}>
      </Box>
    </Container>
  }
  return <>
    <Container component="main" maxWidth="xs">

      <div className="modal-header">
      <span className="close" onClick={close}>&times;</span>
      </div>
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign up
    </Typography>
        <form className={classes.form} noValidate>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                autoComplete="fname"
                name="firstName"
                variant="outlined"
                required
                fullWidth
                id="firstName"
                type="text"
                onChange={(e) => setFirstName(e.target.value)}
                value={first_name}
                label="First Name"
                autoFocus
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="lastName"
                label="Last Name"
                type="text"
                onChange={(e) => setLastName(e.target.value)}
                name="lastName"
                value={last_name}
                autoComplete="lname"
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="email"
                label="Email Address"
                name="email"
                onChange={(e) => setEmail(e.target.value)}
                value={email}
                autoComplete="email"
              />
            </Grid>
            <Grid item xs={12}>
              <div class="form-group" >
                <label >User Type</label>
                <select id="inputState" class="form-control" >
                  <option selected>~ SELECT ~</option>
                  <option>Producer/Director</option>
                </select>
              </div>
            </Grid>

            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="password"
                label="Password"
                type="name"
                onChange={(e) => setPassword(e.target.value)}
                value={password}
                id="password"
                autoComplete="current-password"
              />
              <p>Password must be 8 Characters</p>
              <p>Password must be minimum 1 Uppercase</p>
              <p>Password must be minimum 1 Lowercase</p>
              <p>Password must be minimum 1 Special Character</p>
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="confirm password"
                label="Confirm Password"
                type="password"
                id="password"
              />
            </Grid>
            <Grid item xs={12}>
              <FormControlLabel
                control={<Checkbox value="allowExtraEmails" color="primary" />}
                label="I want to receive inspiration, marketing promotions and updates via email."
              />
            </Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            onClick={signUp}
            className={classes.submit}
          >
            Sign Up
          </Button>
          <Grid container justify="flex-end">
            <Grid item>
              <Link onClick={showLoginScreen} variant="body2">
                Already have an account? Log in
           </Link>
              <h3 className="message">{signup_msg}</h3>
            </Grid>
          </Grid>
        </form>
      </div>
      <Box mt={5}>
      </Box>
    </Container>





  </>
}

export default Login
