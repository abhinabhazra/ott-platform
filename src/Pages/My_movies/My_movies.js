import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import PhoneIcon from '@material-ui/icons/Phone';
import PanToolIcon from '@material-ui/icons/PanTool';
import AccountBalanceIcon from '@material-ui/icons/AccountBalance';
import PhoneIphoneIcon from '@material-ui/icons/PhoneIphone';
import MailOutlineIcon from '@material-ui/icons/MailOutline';
import TextField from '@material-ui/core/TextField';


import "./My_movies.css";



function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`nav-tabpanel-${index}`}
            aria-labelledby={`nav-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={3}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

function a11yProps(index) {
    return {
        id: `nav-tab-${index}`,
        'aria-controls': `nav-tabpanel-${index}`,
    };
}

function LinkTab(props) {
    return (
        <Tab
            component="a"
            onClick={(event) => {
                event.preventDefault();
            }}
            {...props}
        />
    );
}

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.paper,
    },
}));

export default function NavTabs() {
    const classes = useStyles();
    const [value, setValue] = React.useState(0);


    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    return (
        <div className={classes.root}>
            <AppBar position="static">
                <Tabs
                    variant="fullWidth"
                    value={value}
                    onChange={handleChange}
                    aria-label="nav tabs example"
                >
                    <LinkTab className="link_tab" icon={<PhoneIcon />} label="MOBILE & EMAIL" {...a11yProps(0)} />
                    <LinkTab label="PAN" icon={<PanToolIcon />} className="link_tab" {...a11yProps(1)} />
                    <LinkTab label="BANK" icon={<AccountBalanceIcon />} className="link_tab" {...a11yProps(2)} />
                </Tabs>
            </AppBar>
            <TabPanel value={value} index={0}>
                <div className="mobile_tab">  <PhoneIphoneIcon className="phone" />
                    <h3>   Your mobile number is verified</h3>
                    <br />
                    <p className="mobile">6295622155</p></div>
                <br />
                <div className="mobile_tab">  <MailOutlineIcon className="phone" />
                    <h3>   Your Email Address is verified</h3>
                    <br />
                    <p className="mobile">abhinabahazra960@gmail.com</p></div>
            </TabPanel>
            <TabPanel value={value} index={1}>

            <TextField
                            variant="outlined"

                            fullWidth
                            name="PAN"
                            label="Enter Your PAN No."
                            type="number"
                            id="PAN"
                        />
                        <hr/>
                        <TextField
                            variant="outlined"

                            fullWidth
                            type="file" accept="video/image/*" name="img" label="Upload Your PAN image" id="castsss"
                        />
                                        <button id="style_button" type="submit">submit data</button>

                        <hr/>
                <img src="pancard.jfif" alt="pancard" />
                <strong>        <h2>Your PAN is verified</h2>
                </strong>

            </TabPanel>

            <TabPanel value={value} index={2}>
            <div>
                    <h3>  <AccountBalanceIcon className="bank"/>VERIFY BANK ACCOUNT</h3>
                    <p>(Verify your account to withdraw winnings)</p>
                    <button id="style_button1" type="submit">submit data</button>
                    <p>(Bank proof of passbook, Cheque book or bank statement which shows your Name , IFSC Code & Account No.)</p>
                   </div>
            <TextField
                            variant="outlined"

                            fullWidth
                            name="Account"
                            label="Account number"
                            type="password"
                            id="Account"
                        />
                        <hr/>
                        <TextField
                            variant="outlined"

                            fullWidth
                            name="Account"
                            label="Retype Account number"
                            type="number"
                            id="Account"
                        /> 
                        <hr/>
                        <TextField
                            variant="outlined"

                            fullWidth
                            name="PAN"
                            label="IFSC Code"
                            type="number"
                            id="PAN"
                        /> 
                        <hr/>
                        <TextField
                            variant="outlined"

                            fullWidth
                            name="PAN"
                            label="Bank name"
                            type="number"
                            id="PAN"
                        /> 
                        <hr/>
                        <TextField
                            variant="outlined"

                            fullWidth
                            name="PAN"
                            label="Bank branch"
                            type="number"
                            id="PAN"
                        />  
                        <hr/> 
                        <p>All fields are mandatory</p>    </TabPanel>
        </div>
    );
}