import React, { Component } from 'react';
import "./Producer.css";
import {Link} from "react-router-dom";
import AddIcon from '@material-ui/icons/Add';


class Producer extends Component {
    render() {
        return (
            <div>
                <div className="topnav"> <a  id="style_button"><Link to={"/add_movies"}>ADD MOVIES<AddIcon></AddIcon></Link></a>
      <a  id="style_button"><Link to={"/producer_movies"}>MY MOVIES</Link></a>
        <a  id="style_button"><Link to={"/producer_movies"}>MY DRAFTS</Link></a>
        <a  id="style_button"><Link to={"/my_movies"}>BANK ACCOUNT</Link></a>

      </div>
 <img className="img-responsive" src="rill.png" alt="rill" />
 <hr/>
 <img className="img-responsive " id="produ" src="https://res.cloudinary.com/zolapp/image/upload/c_fill,e_sharpen:100,h_1235,q_auto,w_1920/v1613538453/Social_Media_Photo_k8qady.jpg" alt="man"/>
           
 <div class="desc">
<h4 class="heading">You thought, you filmed, now what?</h4>
<p class="description">
A lot stands between a story and a screen. Theatres with all their glamour and OTT platforms with their ever-growing popularity come with challenges. These challenges are also the reasons many great stories remain untold. That's where my CinemaHall comes into the picture.
</p>
</div>
            </div>
        );
    }
}

export default Producer;