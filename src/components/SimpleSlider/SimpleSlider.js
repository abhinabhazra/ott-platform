import React, { Component } from "react";
import Slider from "react-slick";
import "./SimpleSlider.css";

export default class SimpleSlider extends Component {
  render() {
    const settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1
    };
    return (
      <div className="container">
        <Slider {...settings}>
          <div>
          <img src="https://www.weekendworks.in/wp-content/uploads/2016/10/web-1012469_1280.jpg" alt="senapati" />
          </div>
          <div>
          <img src="https://pcrbio.com/app/uploads/Resources-2560-x-480-banner-1920x480.jpg" alt="senapati" />
          </div>
          <div>
          <img src="https://i.pinimg.com/originals/40/b4/85/40b485db011495348be28942ba4318b2.jpg" alt="senapati" />
          </div>
          <div>
          <img src="https://png.pngtree.com/thumb_back/fw800/back_our/20190622/ourmid/pngtree-purple-ray-light-strip-minimalist-banner-background-image_210030.jpg" alt="senapati" />
          </div>
          <div>
          <img src="https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/a1c75dcd-cbcc-4436-8666-2d651b9379d9/de80isw-caff288a-73c1-4b27-8ec2-a1d5991ae659.png/v1/fill/w_1200,h_480,q_80,strp/campfire_banner_commission_by_titaniumgraphics_de80isw-fullview.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOiIsImlzcyI6InVybjphcHA6Iiwib2JqIjpbW3siaGVpZ2h0IjoiPD00ODAiLCJwYXRoIjoiXC9mXC9hMWM3NWRjZC1jYmNjLTQ0MzYtODY2Ni0yZDY1MWI5Mzc5ZDlcL2RlODBpc3ctY2FmZjI4OGEtNzNjMS00YjI3LThlYzItYTFkNTk5MWFlNjU5LnBuZyIsIndpZHRoIjoiPD0xMjAwIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmltYWdlLm9wZXJhdGlvbnMiXX0.0uUDp5PmSXyqKj4cvXfBOvQ_AyIMNIbIXdUYOVMp1LY" alt="senapati" />
          </div>
          <div>
          <img src="https://i.pinimg.com/originals/40/b4/85/40b485db011495348be28942ba4318b2.jpg" alt="senapati" />
          </div>
        </Slider>
      </div>
    );
  }
}