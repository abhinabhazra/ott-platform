import "./Header.css";
import { Link } from "react-router-dom";
import React from "react";
import ExitToAppIcon from '@material-ui/icons/ExitToApp';

class Header extends React.Component{

  constructor(props){
    super(props);
    
  }

  onClickSignOut = () =>{
    sessionStorage.removeItem('auth')
    window.location.reload()
  }

  render(){
    //sessionStorage.removeItem('auth')
    const renderAuthButton = () => {
      if (sessionStorage.getItem('auth')) {
          let {name} = JSON.parse(sessionStorage.getItem('auth'));
          return (
            <>
              <a className="login_in">Welcome {name}</a>
              <a  onClick={this.onClickSignOut }><i className="login_in" ></i>Logout< ExitToAppIcon className="logout_button"/>
</a>
            </>
          )
      } else {
        return(
          <>
            <a className="login_in" onClick={() => {this.props.showLogin(true) }}>LOGIN</a>
            <a className="subscribe" onClick={() => {this.props.showLogin(false) }}>SIGN UP</a>
          </>
        )
      }
    }
    const producerMenue = () => {
        if(sessionStorage.getItem('auth')){
            let {num_master_id} =  JSON.parse(sessionStorage.getItem('auth'));
            // console.log(typeof num_master_id);
            if(num_master_id === '2'){
                console.log('check');
                return( <a><Link to={"/producer"}>Producer</Link></a>);
            }else{
                return null;
            }
        }else{
          return null;
        }
    }
    return(
      <div className="container">
        <span onClick={() => window.scroll(0, 0)} className="header">
      <div class="header_logo">
        <img src="animation_editor.png" alt="movie-plex" />
      </div>
      <div className="topnav"> <a className="active"><Link to={"/"}>HOME</Link></a>
        {/* <a><Link to={"/producer"}>Producer</Link></a> */}
       {producerMenue()}
        {/* <a href="#news">MOVIES</a> */}
      
        {/* <a><Link to={"/user_profile"}>USER PROFILE</Link></a> */}
       
        <a href="https://digimovieplex.com/audience">User Registration</a>

       
        {renderAuthButton()}

      </div>

    </span>      
      </div>
    )
  }

}

export default Header;


